package TableExample;

//------------------------------------------------------------------------
// TableExample.java
// Written by Chuck Cusack, Jan 2002
// A simple application to show how to use a JTable with a Model.
//------------------------------------------------------------------------

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

//------------------------------------------------------------------------
public class TableExample extends JFrame implements ActionListener {

	// ---------------------------------------------------------------------
	PowersModel powersModel = new PowersModel(); // The table model
	JTable myTable = new JTable(powersModel); // The table

	// ---------------------------------------------------
	// Buttons to change the size of the table
	//
	JButton addColumn = new JButton("Add Column");
	JButton addRow = new JButton("Add Row");
	JButton removeColumn = new JButton("Remove Column");
	JButton removeRow = new JButton("Remove Row");

	public static void main(String[] args) {
		TableExample te = new TableExample();
	}

	// ---------------------------------------------------------------------
	public TableExample() {

		// ------------------------------------------------------
		// Make this applet handle the events for the buttons
		//
		addColumn.addActionListener(this);
		addRow.addActionListener(this);
		removeRow.addActionListener(this);
		removeColumn.addActionListener(this);

		// ------------------------------------------------------
		// Place the buttons on a box
		//
		Box buttonBox = Box.createHorizontalBox();
		buttonBox.add(Box.createHorizontalStrut(20));
		buttonBox.add(addColumn);
		buttonBox.add(Box.createHorizontalStrut(20));
		buttonBox.add(removeColumn);
		buttonBox.add(Box.createHorizontalStrut(20));
		buttonBox.add(addRow);
		buttonBox.add(Box.createHorizontalStrut(20));
		buttonBox.add(removeRow);
		buttonBox.add(Box.createHorizontalStrut(20));

		// ------------------------------------------------------
		// Place the table on a scroll pane
		//
		JScrollPane tablePane = new JScrollPane(myTable);
		myTable.setPreferredScrollableViewportSize(new Dimension(400, 400));

		// ------------------------------------------------------------------
		// Place everything on the applet
		//
		JLabel title = new JLabel("Powers of Integers");
		title.setHorizontalAlignment(SwingConstants.CENTER);
		Container cont = this.getContentPane();
		cont.setLayout(new BorderLayout());
		cont.add(title, BorderLayout.NORTH);
		cont.add(tablePane, BorderLayout.CENTER);
		cont.add(buttonBox, BorderLayout.SOUTH);
		// ------------------------------------------------------------------
		pack();
		setSize(400, 400);
		setVisible(true);
	}

	// ---------------------------------------------------------------------
	// When a button is pushed, do as its name indicates
	//
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addColumn) {
			powersModel.addColumn();
		} else if (e.getSource() == addRow) {
			powersModel.addRow();
		} else if (e.getSource() == removeRow) {
			powersModel.removeRow();
		} else if (e.getSource() == removeColumn) {
			powersModel.removeColumn();
		}
	}
	// ---------------------------------------------------------------------
}
// ------------------------------------------------------------------------
