package TableExample;

//----------------------------------------------------
// PowersModel
// Written by Chuck Cusack, Jan 2002
// An implementation of the AbstractTableModel
// An example of how to write a model for use in a
// JTable.  
//----------------------------------------------------
import javax.swing.table.AbstractTableModel;
public class PowersModel extends AbstractTableModel {
    //------------------------------------------------
    // We need to store the number of rows and columns
    int myRows;
    int myCols;

    //------------------------------------------------
    // The first group of methods override methods
    // from the super class so that the model behaves
    // as we wish.
    //------------------------------------------------

    //------------------------------------------------
    // The constructor sets the table sizs to 3 x 3.
    //
    public PowersModel() {
        myRows=3;
        myCols=3;
    }
    //------------------------------------------------
    // Obvious
    //
    public int getColumnCount() {
        return myCols;
    }
    //------------------------------------------------
    // Obvious
    //
    public int getRowCount() {
        return myRows;
    }
    //------------------------------------------------
    // We label the columns with the power it holds.
    //
    public String getColumnName(int i) {
        switch(i) {
          case 0:
            return "n";
          default:
            return "n^"+(i+1);
        }
    }
    //------------------------------------------------
    // Return the value for each cell. 
    // Table cell (row,col) should be (rows+1)^(col+1),
    // since the table represents the first 'col' 
    // powers of the first 'row' integers.  Since we 
    // start indexing at (0,0), we add one to each 
    // dimension.  
    // When the powers become too large, we say so.
    // Notice that we never actually store ANY data in
    // the table.
    //
    public Object getValueAt(int row,int col) {
          double result=Math.pow(row+1,col+1);
          if(result>=Integer.MAX_VALUE) {
            return "too big";
          }
          else  {
            return ""+(int)result;
          }
    }

    //------------------------------------------------
    // The following methods are "add-ons".  That is,
    // they have nothing to do with the model itself,
    // but allow us to change some of its features.
    // Notice at the end of each we call 
    //    fireTableStructureChanged().
    // We need to do this to tell the JTable to update
    // its view of the data.
    //------------------------------------------------
    // Set the number of columns to display
    //
    public void setColumns(int i) {
        myCols=i;
        fireTableStructureChanged();
    }
    //------------------------------------------------
    // Increase the number of columns by one
    //
    public void addColumn() {
        myCols++;
        fireTableStructureChanged();
    }
    //------------------------------------------------
    // Decrease the number of columns by one
    //
    public void removeColumn() {
        if(myCols>1) 
           myCols--;
        fireTableStructureChanged();
    }
    //------------------------------------------------
    // Set the number of rows to display
    //
    public void setRows(int i) {
        myRows=i;
        fireTableStructureChanged();
    }
    //------------------------------------------------
    // Increase the number of rows by one
    //
    public void addRow() {
        myRows++;
        fireTableStructureChanged();
    }
    //------------------------------------------------
    // Decrease the number of rows by one
    //
    //
    public void removeRow() {
        if(myRows>1) 
           myRows--;
        fireTableStructureChanged();
    }
    //------------------------------------------------
}
