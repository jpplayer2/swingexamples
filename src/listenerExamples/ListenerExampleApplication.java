package listenerExamples;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class ListenerExampleApplication extends JFrame {

	/**
	 * The main method, which allows us to run the application without using a
	 * webpage. In other words, this is the method that is called when you run a
	 * Java application.
	 */
	public static void main(String[] args) {
		new ListenerExampleApplication();
	}

	/**
	 * In applets, the init method is where things get set up. This is the only
	 * method that is needed if it is run as an applet in a webpage.
	 */
	public ListenerExampleApplication() {
		setTitle("A useless application");

		// This must be done so that the application exits when the window
		// is closed. If this (or several other alternatives) is not done,
		// the application will still reside on your system. That is, it
		// won't look like it is running any more, but it will be.
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Instantiate the main drawing panel
		ListenerExamplePanel mainPanel = new ListenerExamplePanel();

		// Place all of the graphical components on the main window
		Container cont = getContentPane();
		// cont.setLayout(new BorderLayout());
		cont.add(mainPanel, BorderLayout.CENTER);

		// Finish setting up the main window
		setBackground(Color.white);
		pack();
		setSize(new Dimension(600, 100));
		setVisible(true);

		add(new ListenerExamplePanel());
	}
}
