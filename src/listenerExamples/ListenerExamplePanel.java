package listenerExamples;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * ListenerExamplePanel is an example of different ways of implementing
 * listeners on an object.
 * 
 * @author cusack
 *
 */
public class ListenerExamplePanel extends JPanel implements ActionListener {
	
	/* 
	 * These need to be class variable since they need to be accessed
	 * by various actionPerformed methods.
	 */
    JTextField inField=new JTextField("",4);
    JTextField outField=new JTextField("",30);
    
    /**
     * A constructor
     */
	 public ListenerExamplePanel() {
		 
		    // Just a simple layout for this example
	        setLayout(new FlowLayout());

	        // The 3 buttons on the applet.  Because of the way we are
	        // doing the listening, we do not need these to be class variables.
	        JButton b1=new JButton("x");
	        JButton b2=new JButton("y");
	        JButton b3=new JButton("z");
	        
	        // We put the fields and buttons onto the applet.
	        add(inField);
	        add(b1);
	        add(b2);
	        add(b3);
	        add(outField);
	       
	        // For the first button, we have this class handle it.
	        b1.addActionListener(this);
	        
	        // For the second button, we use an anonymous inner class to handle it.
	        b2.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	       		    outField.setText("The input was: "+inField.getText()+". The 'y' button was clicked");
	       	    }
	        });
	        
	        // For the third button, we use another class which implements the ActionListener interface.
	        // This class is defined below as an inner class so it can access the class variables.
  
	        MyListener b3Listener = new MyListener();
	        b3.addActionListener(b3Listener);
	        
	        // Of course, we can have more than one listener for an object.
	        // We will add another listener to the third button.
	        // In this case, the class is defined elsewhere.
	        SomeListener anotherB3Listener = new SomeListener();
	        b3.addActionListener(anotherB3Listener);
	 }
	 
	 /**
	  * The actionPerfomed method required by the ActionListener interface.
	  */
	 public void actionPerformed(ActionEvent e) {
		 outField.setText("The input was: "+inField.getText()+". The 'x' button was clicked");
	 }
	 
	 /**
	  * MyListener is a private inner class that can be used within this class.
	  *  The sole purpose of this class is to handle events.  It is an inner class because
	  *  we need to access the outField and inField variables.
	  * 
	  * @author cusack
	  */
	 private class MyListener implements ActionListener {

		 /**
		  * The actionPerfomed method required by the ActionListener interface.
		  */
		 public void actionPerformed(ActionEvent e) {
			 outField.setText("The input was: "+inField.getText()+". The 'z' button was clicked");
		 }
	 }
}
